export const baseServiceAddress = () => {
  const hostname = window && window.location && window.location.hostname;
  if (hostname === "localhost" || hostname.indexOf("192.168.") === 0) {
    return `${hostname}:5005`;
  } else {
    return window.location.host;
  }
};

export const serviceEndpoint = () => {
  return "http://" + baseServiceAddress() + "/checkbox";
};

export const toggleServiceEndpoint = () => {
  return serviceEndpoint() + "/toggle";
};

export const socketEndpoint = () => {
  return "ws://" + baseServiceAddress() + "/ws";
};
