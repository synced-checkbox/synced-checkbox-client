import "./App.css";
import React, { useEffect, useState } from "react";
import * as AppConstants from "./AppConstants";

function App() {
  const [checked, setChecked] = useState(null);
  const [lastStateNumber, _setLastStateNumber] = useState(0);
  const setLastStateNumber = (number) => {
    _setLastStateNumber(number);
    lastStateNumberRef.current = number;
  };

  const lastStateNumberRef = React.useRef(lastStateNumber);

  const createNewSocket = () => {
    const newSocket = new WebSocket(AppConstants.socketEndpoint());
    newSocket.onmessage = (event) => {
      const splitData = event.data.split(",");
      const stateNumber = parseInt(splitData[1]);
      const stateNumberDiff = Math.abs(
        stateNumber - lastStateNumberRef.current
      );

      if (
        stateNumber > lastStateNumberRef.current ||
        stateNumberDiff > 100000
      ) {
        if (splitData[0] === "True") {
          setChecked(true);
          setLastStateNumber(stateNumber);
        } else if (splitData[0] === "False") {
          setChecked(false);
          setLastStateNumber(stateNumber);
        }
      }
    };
    return newSocket;
  };

  useEffect(() => {
    const socket = createNewSocket();
    setInterval(pingSocket(socket), 10000);
  }, []);

  const pingSocket = (mySocket) => {
    let socket = mySocket;
    return () => {
      if (socket.readyState === socket.OPEN) {
        socket.send("ping");
      } else if (socket.readyState !== socket.CONNECTING) {
        const newSocket = createNewSocket();
        socket = newSocket;
      }
    };
  };

  const toggleCheckBox = () => {
    setChecked(!checked);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", AppConstants.toggleServiceEndpoint(), true);
    xhr.send();
  };

  return (
    <div className="App">
      <div className="BoxWrapper">
        {checked === null && <div className="LoadingText">Connecting...</div>}
        {checked !== null && (
          <input type="checkbox" checked={checked} onChange={toggleCheckBox} />
        )}
      </div>
    </div>
  );
}

export default App;
